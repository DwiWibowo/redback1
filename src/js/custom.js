
// chat toggle (maximize/minimze)
$(".chat-toggle").on("click", function(){
	$(this).parents(".chat-box").toggleClass("is-minimized");
});

// make participant list are dragable
//  $('.content-area tbody').sortable();

//  slick slider
$('.slides').slick({
	slidesToShow: 8,
	infinite: false,
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 5
			}
		}
	] 
});

// accordion
$('.accordion-header').on('click', function (){
	$(this).parent().toggleClass('is-active');
});

$(window).resize(function(){
	if ($(window).width() <= 1280) {
		sliderWidth();
	}
});
function sliderWidth() {
	var sidebarWidth = 360;
	var windowWidth = $(window).width();
	var slideWidth = windowWidth - sidebarWidth;
	$('.fixet-bottom').css('max-width', slideWidth);
}