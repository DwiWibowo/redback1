var gulp = require('gulp'),
sass = require('gulp-sass'),
uglify = require('gulp-uglify'),
cleanCSS = require('gulp-clean-css'),
concat = require('gulp-concat'),
header = require('gulp-header'),
sourcemaps = require('gulp-sourcemaps'),
plumber = require('gulp-plumber'),
notify = require('gulp-notify'),
merge = require('merge-stream'),
data = require('gulp-data'),
nunjucks = require('gulp-nunjucks-render');

var fs = require('fs');
var pkg = JSON.parse(fs.readFileSync('package.json'));

// Task options
var opts = {
	assetsPath: './src',
	minRename: {
		suffix: '.min',
	},
	banner: [
		'/*!',
		'Style Name: Prelim Webcast',
		'Description: Prelim Webcast HTML template',
		'Version: 1.0',
		'Author: Dwi Wibowo',
		'Author URI: #',
		'*/\n',
	].join('\n'),
	css_files: [],
	js_files: [
		'src/js/slick.js',
		'src/js/custom.js'
	]
};

// Nunjucks Task
// Combine HTML
gulp.task('nunjucks', function(){
	gulp.src('./src/templates/*.html')
		.pipe(data(function() {
			return require('./src/data.json')
		}))
		.pipe(nunjucks({
			path: ['./src/templates/']
		}))
		// .pipe(removeLine())
		.pipe(gulp.dest('./'))
});

// Styles Task
gulp.task('styles', function() {

	var mainCss = gulp.src(['./src/sass/style.sass'])
		.pipe(plumber({errorHandler: function(err){
			notify.onError({
				title: "Gulp Error in" + err.plugin,
				message: err.toString()
			})(err)
		}}))
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}))

	var depsCss = gulp.src( opts.css_files )
		.pipe(cleanCSS())
		.pipe(plumber());

	return merge(depsCss, mainCss)
		.pipe(concat('style.css'))
		.pipe(header(opts.banner, pkg))
		.pipe(sourcemaps.write('style-maps'))
		.pipe(gulp.dest('./dist/css/'))
});

// Scripts Task
// Uglifies
gulp.task('scripts', function() {

	gulp.src( opts.js_files )
	.pipe(uglify())
	.pipe(plumber())
	.pipe(concat('main.min.js'))
	.pipe(gulp.dest('./dist/js/'))
});

// Task Task
// Watches JS
gulp.task('watch', function() {
	gulp.watch('src/templates/*.html', ['nunjucks']);
	gulp.watch('src/js/*.js', ['scripts']);
	gulp.watch(['src/sass/**/**.sass','src/sass/**/**.scss'], ['styles']);
});

// Default Task
// Gulp
gulp.task('default', ['nunjucks', 'styles', 'scripts', 'watch']);